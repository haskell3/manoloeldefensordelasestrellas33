using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChange : MonoBehaviour
{
    public Options options;
    public Slider r;
    public Slider g;
    public Slider b;
    public GameEvent changecolor;

    public void ChangeColor()
    {
        Color col = new Color();
        col.r = r.value;
        col.g = g.value;
        col.b = b.value;
        col.a = 255;
        options.playerColor = col;
        changecolor.Raise();
    }
}
