using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CooldownController : MonoBehaviour
{
    [SerializeField]
    private Image imageCooldown;
    [SerializeField]
    private TextMeshProUGUI textCooldown;
    [SerializeField]
    private GameEvent FinishCooldown;

    private float cooldownTime = 3.0f;
    private float cooldownTimer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    IEnumerator ApplyCooldown(float time)
    {
        cooldownTime = time;
        cooldownTimer = cooldownTime;

        while (cooldownTimer > 0.0f)
        {
            yield return new WaitForSeconds(0.1f);
            cooldownTimer -= 0.1f;
            textCooldown.text = Mathf.RoundToInt(cooldownTimer).ToString();
            imageCooldown.fillAmount = cooldownTimer / cooldownTime;
        }
        imageCooldown.fillAmount = 0.0f;
        textCooldown.text = "";
        FinishCooldown.Raise();
    }

    public void StartCooldown(float time)
    {
        StartCoroutine(ApplyCooldown(time));
    }
}
