using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Options : ScriptableObject
{
    public Color playerColor;
    public float musicVolume;
}
