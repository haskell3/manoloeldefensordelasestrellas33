using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSliderController : MonoBehaviour
{
    [SerializeField]
    private Options options;
    [SerializeField]
    private GameEvent changeMusic;
    
    public void ChangeMusic()
    {
        options.musicVolume = gameObject.GetComponent<Slider>().value;
        changeMusic.Raise();
    }
}
