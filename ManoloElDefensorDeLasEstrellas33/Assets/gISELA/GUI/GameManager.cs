using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    public GameObject menu;
    [SerializeField]
    public GameObject options;

    public void ChangeToMenu()
    {
        menu.gameObject.SetActive(true);
        options.gameObject.SetActive(false);
    }
    public void ChangeToOptions()
    {
        menu.gameObject.SetActive(false);
        options.gameObject.SetActive(true);
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Level1");
    }
}
