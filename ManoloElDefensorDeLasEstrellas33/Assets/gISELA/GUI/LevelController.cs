using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    [SerializeField]
    private int level;
    [SerializeField]
    private Escenas escenasinfo;
    // Start is called before the first frame update
    void Start()
    {
        escenasinfo.numScene = level;
        gameObject.GetComponent<TextMeshProUGUI>().text = "Level: " + level;
    }
}
