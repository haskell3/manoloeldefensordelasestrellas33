using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPlayerDemo : MonoBehaviour
{
    public Options opt;
    
    public void ChangeColor()
    {
        
        gameObject.GetComponent<Image>().color = opt.playerColor;
    }
}
