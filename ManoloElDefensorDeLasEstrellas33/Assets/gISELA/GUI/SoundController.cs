using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    [SerializeField]
    private Options options;

    private void Awake()
    {
        ChangeVolumeMusic();
    }
    public void ChangeVolumeMusic()
    {
        gameObject.GetComponent<AudioSource>().volume = options.musicVolume;
    }
}
