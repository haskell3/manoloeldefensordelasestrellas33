using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonController : MonoBehaviour
{
    [SerializeField]
    private GameEvent abrirPuerta;
    private bool playerNear = false;
    [SerializeField]
    private GameObject InteractionText;

    public void CanPulse()
    {
        if(playerNear)
        {
            PulsarBoton();
        }
    }
    private void PulsarBoton()
    {
        Debug.Log("Se ha pulsado el boton");
        abrirPuerta.Raise();
        InteractionText.SetActive(false);
        gameObject.SetActive(false);
        
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("enter" + other.name);
        Debug.Log("player dentro");
        playerNear = true;
        InteractionText.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        Debug.Log("exit" + other.name);
        playerNear = false;
        InteractionText.SetActive(false);
        Debug.Log("player fuera");
    }

}
