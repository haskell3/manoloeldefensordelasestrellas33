using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BarreraController : MonoBehaviour
{
    [SerializeField]
    private Escenas escenas;
    private void OnTriggerEnter(Collider other)
    {
        escenas.numScene++;
        SceneManager.LoadScene(escenas.numScene);
    }
}
