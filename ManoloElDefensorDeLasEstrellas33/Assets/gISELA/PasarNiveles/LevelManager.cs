using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [Header("Player")]
    [SerializeField]
    private List<Vector3> checkpoints;
    [SerializeField]
    private int currentCheckpoint;
    [SerializeField]
    private GameObject Player;
    [Header("Robot")]
    [SerializeField]
    private Vector3 robotRespawn;
    [SerializeField]
    private GameObject Robot;

    public void ChangeCheckPoint()
    {
        if (checkpoints.Count-1 > currentCheckpoint)
        {
            ++currentCheckpoint;
        }
    }

    public void RespawnPlayer()
    {
        Player.transform.position = checkpoints[currentCheckpoint];
        //Player.GetComponent<MovimentPlayer>() //Aqui deberia llamar a StopGrapp
    }

    public void RespawnRobot()
    {
        StartCoroutine(RobotMorir());
    }

    IEnumerator RobotMorir()
    {
        yield return new WaitForSeconds(5f);
        GameObject newRobot = Instantiate(Robot);
        newRobot.transform.position = robotRespawn;
    }
}