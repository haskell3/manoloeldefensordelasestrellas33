using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Escenas : ScriptableObject
{
    public int numScene;
}
