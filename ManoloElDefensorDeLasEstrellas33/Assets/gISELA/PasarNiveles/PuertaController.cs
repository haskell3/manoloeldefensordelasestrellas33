using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PuertaController : MonoBehaviour
{
    private enum SwitchMachineStates { NONE, CLOSED, OPENING, CLOSING };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;

    [SerializeField]
    private Animator animator;

    /*private float m_ActualIncrement = 0;

    [SerializeField]
    private float m_Increment;
    [SerializeField]
    private float m_Time;
    [SerializeField]
    private float m_MicroIncrement;*/

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.CLOSED:
                animator.Play("Closed");
                break;

            case SwitchMachineStates.OPENING:
                animator.Play("Opening");
                //StartCoroutine(AnimationOpenDoor());
                break;
            case SwitchMachineStates.CLOSING:
                animator.Play("CloseDoor");
                break;

            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.CLOSED:

                break;

            case SwitchMachineStates.OPENING:

                break;
            case SwitchMachineStates.CLOSING:

                break;

            default:
                break;
        }
    }

    private void Start()
    {
        InitState(SwitchMachineStates.CLOSED);
    }

    public void OpenDoor()
    {
        Debug.Log("sE HA ABIERTO LA PUERTA");
        ChangeState(SwitchMachineStates.OPENING);
    }
    public void CloseDoor()
    {
        Debug.Log("Se ha cerrado la puerta");
        ChangeState(SwitchMachineStates.CLOSING);
    }

    /*private IEnumerator AnimationOpenDoor()
    {
        while (m_ActualIncrement < m_Increment)
        {
            Vector3 newPosition = transform.position;
            newPosition.y += m_MicroIncrement;
            transform.position = newPosition;
            m_ActualIncrement += m_MicroIncrement;
            yield return new WaitForSeconds(m_Time);
        }
    }*/
}
