using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VistaController : MonoBehaviour
{
    [SerializeField]
    private GameObject ayudante;
    private void OnTriggerEnter(Collider other)
    {
        ayudante.GetComponent<AyudanteController>().CheckWall(other.transform.position);
        Debug.Log("Te veo " + other.gameObject.name);
    }
    private void OnTriggerExit(Collider other)
    {
        ayudante.GetComponent<AyudanteController>().FueraPlayer();
        Debug.Log("ya no te veo");
    }
}
