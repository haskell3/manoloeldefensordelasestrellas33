using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlacaController : MonoBehaviour
{
    [SerializeField]
    private GameEvent Muelto;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("presionada");
        Muelto.Raise();
    }
}
