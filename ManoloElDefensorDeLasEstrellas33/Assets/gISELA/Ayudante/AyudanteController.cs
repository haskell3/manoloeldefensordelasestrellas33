using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;

public class AyudanteController : MonoBehaviour, IExplosable
{
    //[SerializeField]
    //GameObject m_prefab;
    [SerializeField]
    private float m_RotationSpeed;
    [SerializeField]
    private float m_Speed;
    [SerializeField]
    private LayerMask m_LayerMask;
    private NavMeshPath currentpath;
    [SerializeField]
    private LayerMask m_VistaLayerMask;
    [SerializeField]
    private int currentcorner;
    [SerializeField]
    private GameObject m_vista;
    private float m_SQDistancePerFrame;
    private Rigidbody m_RigidBody;
    [SerializeField]
    private GameObject m_rootSuelo;
    [SerializeField]
    private GameObject m_hips;
    private Animator m_Animator;
    private Rigidbody m_Rigidbody;
    [SerializeField]
    private GameEvent RobotDead;

    private enum SwitchMachineStates { NONE, IDLE, WALK, DISTRACTED, DEAD };
    [SerializeField]
    private SwitchMachineStates m_CurrentState;
    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log("Robotin: "+newState.ToString());
    }
    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Animator.Play("Idle");

                break;

            case SwitchMachineStates.WALK:
                m_Animator.Play("Walk_N");

                break;

            case SwitchMachineStates.DEAD:
                m_vista.gameObject.SetActive(false);
                GetComponent<Ragdoll>().Activate(true);
                StartCoroutine(RobotMorir());
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                
                break;

            case SwitchMachineStates.WALK:

                break;
            case SwitchMachineStates.DEAD:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                gameObject.transform.Rotate(Vector3.up * m_RotationSpeed * Time.deltaTime);

                break;
            case SwitchMachineStates.WALK:

                RaycastHit hit;
                if (!Physics.Raycast(m_rootSuelo.transform.position, -transform.up, out hit, 5, m_LayerMask))
                {
                    ChangeState(SwitchMachineStates.IDLE);
                    break;
                }
                if (currentpath.corners.Length > 0)
                {
                    Vector3 m_Movement = (currentpath.corners[currentcorner] - hit.point).normalized;
                    m_RigidBody.velocity = m_Movement * m_Speed;
                    Debug.DrawLine(transform.position, transform.position + m_RigidBody.velocity, Color.red);
                    Debug.DrawLine(hit.point, currentpath.corners[currentcorner], Color.magenta);
                    //Debug.Log($"{Vector3.Distance(currentpath.corners[currentcorner], hit.point)} ~ {currentpath.corners[currentcorner].y} --- {hit.point.y}");
                    if (Vector3.Distance(currentpath.corners[currentcorner], hit.point) <= m_SQDistancePerFrame)
                    {
                        if (currentcorner < currentpath.corners.Length - 1)
                            ++currentcorner;
                        else
                            ChangeState(SwitchMachineStates.IDLE);
                    }
                }
                else
                {
                    ChangeState(SwitchMachineStates.IDLE);
                }
               


                break;
            case SwitchMachineStates.DEAD:

                break;
            default:
                break;
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 10)
        {
            Debug.Log("El robot se ha morido");
            RobotDead?.Raise();
        }
    }

    private void Awake()
    {
        m_SQDistancePerFrame = 3 * m_Speed * Time.fixedDeltaTime;
        //m_SQDistancePerFrame *= m_SQDistancePerFrame;
        currentpath = new NavMeshPath();
        m_RigidBody = gameObject.GetComponent<Rigidbody>();
        m_Animator = GetComponent<Animator>();
    }
    void Start()
    {
        //DetectaPlayer(new Vector3(18, 2, -16));
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
    }

    private void DetectaPlayer(Vector3 playerpos)
    {
        NavMesh.CalculatePath(transform.position, playerpos, NavMesh.AllAreas, currentpath);
        /*foreach (Vector3 node in currentpath.corners)
        {
            GameObject nodeDisplay = Instantiate(m_prefab);
            nodeDisplay.transform.position = node;
            Debug.Log(node);
        }*/
        ChangeState(SwitchMachineStates.WALK);
    }

    public void CheckWall(Vector3 playerpos)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, playerpos-transform.position, out hit, 50, m_VistaLayerMask))
        {
            if (hit.collider.tag == "Player")
            {
                DetectaPlayer(playerpos);
                
            }
        }
        
    }

    public void FueraPlayer()
    {
        currentpath.ClearCorners();
        currentcorner = 0;
        ChangeState(SwitchMachineStates.IDLE);
    }

    public void Morirse()
    {
        Debug.Log("me muero");
        GetComponent<CapsuleCollider>().enabled = false;
        m_Animator.enabled = false;
        gameObject.AddComponent<Ragdoll>();
        m_hips.SetActive(true);
        ChangeState(SwitchMachineStates.DEAD);
    }

    IEnumerator RobotMorir()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }

    public void Explode(Vector3 force)
    {
        //m_Rigidbody.AddForce(force, ForceMode.Impulse);
    }
}
