using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameEventFloat : ScriptableObject
{
    private readonly List<GameEventListenerFloat> eventListeners =
        new List<GameEventListenerFloat>();

    public void Raise(float a)
    {
        for (int i = eventListeners.Count - 1; i >= 0; i--)
            eventListeners[i].OnEventRaised(a);
    }

    public void RegisterListener(GameEventListenerFloat listener)
    {
        if (!eventListeners.Contains(listener))
            eventListeners.Add(listener);
    }

    public void UnregisterListener(GameEventListenerFloat listener)
    {
        if (eventListeners.Contains(listener))
            eventListeners.Remove(listener);
    }

}
