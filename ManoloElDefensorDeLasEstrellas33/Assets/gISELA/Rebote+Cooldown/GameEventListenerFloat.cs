using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEventListenerFloat : MonoBehaviour
{
    [Tooltip("Event to register with.")]
    public GameEventFloat Event;

    [Tooltip("Response to invoke when Event is raised.")]
    public UnityEvent<float> Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);
    }

    private void OnDisable()
    {
        Event.UnregisterListener(this);
    }

    public void OnEventRaised(float a)
    {

        Response.Invoke(a);
    }
}