using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.Rendering.DebugUI;

public class PunteroMomento : MonoBehaviour
{
    [SerializeField]
    private Color colorSiHook;
    [SerializeField]
    private Color colorNoHook;
    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    public void IsHookable(bool hook)
    {
        if (hook)
            image.color = colorSiHook;
        else
            image.color = colorNoHook;
    }
}
