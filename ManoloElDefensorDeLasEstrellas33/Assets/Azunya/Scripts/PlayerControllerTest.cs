using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

public class PlayerControllerTest : MonoBehaviour, IExplosable
{


    [Header("Layers")]
    [SerializeField]
    private LayerMask m_FloorLayer;
    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private LayerMask m_ExplosionMask;

    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private Rigidbody m_Rigidbody;
    [Header("VFX")]
    [SerializeField]
    private VisualEffect[] m_Vfxs;

    private Vector2 m_Movement = Vector2.zero;
    [Header("Character Values")]
    [SerializeField]
    private float m_Speed = 2;
    [SerializeField]
    private float m_Acceleration = 2;
    [SerializeField]
    private float m_Drag = 2;
    [SerializeField]
    private float m_DragAir = 2;
    [SerializeField]
    private float m_JumpForce = 2;
    [SerializeField]
    private float m_JumpLess = 2;
    [SerializeField]
    private int m_JumpLoops = 2;
    [SerializeField]
    private float m_Gravity = 2;
    [SerializeField]
    private float m_RadiusExplosion = 2;
    [SerializeField]
    private float m_ExplosionPower = 2;
    [SerializeField]
    private float m_SmokeDuration = 2.5f;
    /*[SerializeField]
    private int m_Hit1Damage = 2;
    [SerializeField]
    private int m_Hit2Damage = 5;*/
    [SerializeField]
    private float cooldown = 3;
    //Deprecated
    [SerializeField]
    private GameEventFloat ActiveCooldownInt;
    [SerializeField]
    private GameEvent GameStarted;
    [SerializeField]
    private float ReboundPower = 2;

    [Header("Hook Values")]
    [SerializeField]
    private Vector3 grapplePoint;
    [SerializeField]
    private LayerMask whatIsGrappleable;
    [SerializeField]
    private float maxDistance = 100f;
    [SerializeField]
    private float spring = 4.5f;
    [SerializeField]
    private float damper = 7f;
    [SerializeField]
    private float massScale = 4.5f;
    [SerializeField]
    private float breakForce = 4.5f;
    private SpringJoint joint;
    private LineRenderer lr;

    private bool m_CanJump = true;
    private bool cannonactive = true;
    private Vector3 lastForce;
    private enum SwitchMachineStates { NONE, IDLE, WALK, FLYING, CLIMBING };
    private SwitchMachineStates m_CurrentState;

    

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log(newState.ToString());
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //float yVelocity = m_Rigidbody.velocity.y;
                //m_Rigidbody.velocity = new Vector3( 0, yVelocity, 0);

                break;

            case SwitchMachineStates.WALK:

                break;
            case SwitchMachineStates.FLYING:

                break;
            case SwitchMachineStates.CLIMBING:

                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;
            case SwitchMachineStates.FLYING:

                break;
            case SwitchMachineStates.CLIMBING:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if (m_Movement != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                /*if(m_Rigidbody.velocity.y != 0)
                {
                    Debug.Log(m_Rigidbody.velocity.y);
                    ChangeState(SwitchMachineStates.FLYING);
                }*/
                break;
            case SwitchMachineStates.WALK:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if (m_Movement == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                /*if (m_Rigidbody.velocity.y != 0)
                {
                    Debug.Log(m_Rigidbody.velocity.y);
                    ChangeState(SwitchMachineStates.FLYING);
                }*/
                break;
            case SwitchMachineStates.FLYING:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                break;
            case SwitchMachineStates.CLIMBING:

                break;
            default:
                break;
        }

    }
    private void FixedUpdate()
    {
        Vector3 groundSpeed = Vector3.right * m_Rigidbody.velocity.x + Vector3.forward * m_Rigidbody.velocity.z;
        Vector3 movement = (transform.forward * m_Movement.y + transform.right * m_Movement.x).normalized;

        if (groundSpeed.magnitude < m_Speed)
        {
            if (!Physics.Raycast(transform.position, movement, .7f, m_ShootMask))
            {
                m_Rigidbody.AddForce((movement * m_Speed) * m_Acceleration, ForceMode.Acceleration);
            }
        }

        groundSpeed = Vector3.right * m_Rigidbody.velocity.x + Vector3.forward * m_Rigidbody.velocity.z;
        //Floor drag
        if (m_CurrentState != SwitchMachineStates.FLYING)
            m_Rigidbody.AddForce(-groundSpeed * m_Drag, ForceMode.Acceleration);
        else
            m_Rigidbody.AddForce(-groundSpeed * m_DragAir, ForceMode.Acceleration);
        //Air drag ^^

        //Gravity
        m_Rigidbody.AddForce(Vector3.up * -1 * m_Gravity, ForceMode.Acceleration);

    }

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Assert.IsNotNull(m_InputAsset);
        
        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Jump").started += StartJumping;
        m_Input.FindActionMap("Default").FindAction("Jump").canceled += EndJumping;
        m_Input.FindActionMap("Default").FindAction("ChangeCamera").started += changeCamera;
        m_Input.FindActionMap("Default").FindAction("Explosion").performed += Explosion;
        m_Input.FindActionMap("Menu").FindAction("Start").performed += changeActionMap;
        m_Input.FindActionMap("Default").FindAction("Hook").performed += StartGrapple;
        m_Input.FindActionMap("Default").FindAction("Hook").canceled += StopGrapple;

        //m_Input.FindActionMap("Default").FindAction("Attack").performed += AttackAction;
        m_Input.FindActionMap("Menu").Enable();

        m_Rigidbody = GetComponent<Rigidbody>();

        lr = GetComponent<LineRenderer>();
        lr.positionCount = 0;
    }

    private void Explosion(InputAction.CallbackContext context)
    {
        if (cannonactive)
        {
            RaycastHit hit;

            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, Mathf.Infinity, m_ShootMask))
            {
                foreach (VisualEffect vfx in m_Vfxs)
                {
                    vfx.transform.position = hit.point;
                    vfx.enabled = true;
                    vfx.Play();
                }
                StartCoroutine(StopExplosion());
                RaycastHit[] hits = Physics.SphereCastAll(hit.point, m_RadiusExplosion, Vector3.up, 1, m_ExplosionMask);

                foreach (RaycastHit exploded in hits)
                {
                    Debug.DrawLine(exploded.rigidbody.transform.position, hit.point, Color.red, 3f);
                    IExplosable ie = exploded.rigidbody.GetComponent<IExplosable>();

                    if (ie == null) continue;

                    Vector3 power = exploded.rigidbody.transform.position - hit.point;

                    power = power.normalized;

                    if (exploded.distance > 0)
                    {
                        power *= (m_ExplosionPower * (m_RadiusExplosion / exploded.distance));
                    }
                    else
                    {
                        power *= (m_ExplosionPower * m_RadiusExplosion);
                    }


                    ie.Explode(power);
                }
            }
            cannonactive = false;
            ActiveCooldownInt.Raise(cooldown);
        }
    }

    private IEnumerator StopExplosion()
    {
        yield return new WaitForSeconds(m_SmokeDuration);
        foreach (VisualEffect vfx in m_Vfxs)
        {
            vfx.enabled = false;
        }
    }

    private void EndJumping(InputAction.CallbackContext context)
    {
        StopAllCoroutines();
    }

    private void StartJumping(InputAction.CallbackContext context)
    {
        if (!m_CanJump) return;
        if (m_CurrentState == SwitchMachineStates.FLYING)
        {
            m_CanJump = false;
        }
        ChangeState(SwitchMachineStates.FLYING);
        StartCoroutine(MoreJump());
    }

    private IEnumerator MoreJump()
    {
        float mult = 1f;
        for (int i = m_JumpLoops; i >= 0; i--)
        {
            m_Rigidbody.AddForce(transform.up * m_JumpForce * mult);
            mult -= m_JumpLess;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void changeCamera(InputAction.CallbackContext context)
    {
        m_Camera.GetComponent<TestCamera>().NegateX();
    }

    private void changeActionMap(InputAction.CallbackContext context)
    {
        m_Input.FindActionMap("Menu").Disable();
        m_Input.FindActionMap("Default").Enable();
        m_Camera.GetComponent<TestCamera>().enabled = true;
        GameStarted?.Raise();
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
        if (m_Rigidbody.velocity != Vector3.zero) lastForce = m_Rigidbody.velocity;

    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Default").Disable();
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Layer from floors
        if (collision.gameObject.layer == 3)
        {
            ChangeState(SwitchMachineStates.WALK);
            m_CanJump = true;
        }
        //Layer from walls
        if (collision.gameObject.layer == 7)
        {
            Debug.Log("Te la devuelvo " + lastForce);
            if (lastForce.x > 12 || lastForce.z > 12 || lastForce.y > 12)
            {
                Debug.Log("Boing");
                var direction = Vector3.Reflect(lastForce.normalized, collision.contacts[0].normal);
                //Vector3 rebote = new Vector3(lastForce.x * -ReboundPower, lastForce.y * ReboundPower, lastForce.z * -ReboundPower);
                Vector3 rebote = lastForce.magnitude * direction * ReboundPower;
                m_Rigidbody.AddForce(rebote, ForceMode.Impulse);
            }

        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //Layer from floors
        if (collision.gameObject.layer == 3)
        {
            ChangeState(SwitchMachineStates.FLYING);
        }
    }

    public void Explode(Vector3 force)
    {
        m_Rigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void ActivateCannon()
    {
        cannonactive = true;
    }

    //Called after Update
    void LateUpdate()
    {
        DrawRope();
    }



    void StartGrapple(InputAction.CallbackContext context)
    {
        Debug.Log("Grabbing");
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, maxDistance, whatIsGrappleable))
        {
            grapplePoint = hit.point;
            joint = gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;
            joint.breakForce = breakForce;

            float distanceFromPoint = Vector3.Distance(transform.position, grapplePoint);

            joint.maxDistance = distanceFromPoint * 0.8f;
            joint.minDistance = distanceFromPoint * 0.25f;
            joint.spring = spring;
            joint.damper = damper;
            joint.massScale = massScale;

            lr.positionCount = 2;
            currentGrapplePosition = transform.position;
        }
    }

    void StopGrapple(InputAction.CallbackContext context)
    {
        Debug.Log("Stop it");
        lr.positionCount = 0;
        Destroy(joint);
    }

    private Vector3 currentGrapplePosition;

    void DrawRope()
    {
        //If not grappling, don't draw rope
        if (!joint) return;

        currentGrapplePosition = Vector3.Lerp(currentGrapplePosition, grapplePoint, Time.deltaTime * 8f);

        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, currentGrapplePosition);
    }

    public bool IsGrappling()
    {
        return joint != null;
    }

    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }
}