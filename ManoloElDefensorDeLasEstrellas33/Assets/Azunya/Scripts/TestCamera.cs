using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.InputSystem;

public class TestCamera : MonoBehaviour
{
    [Header("Input")]
    [SerializeField]
    private InputActionAsset m_InputAsset;
    private InputActionAsset m_Input;
    private InputActionMap m_CurrentActionMap;
    private InputAction m_MouseAction;
    private InputAction m_ZoomAction;

    [Header("Player Values")]
    [SerializeField]
    private Transform player;

    [Header("Camera Values")]
    private Vector2 input;
    [SerializeField]
    private Transform camera;
    [SerializeField]
    private float offsetX = 0.5f;
    [SerializeField]
    private float offsetY = 1f;
    [SerializeField]
    private bool canMove = true;
    [SerializeField]
    private bool offsetXnegate = true;
    [SerializeField]
    private bool debug = true;
    //CameraZoom
    [SerializeField]
    private float zoomSpeed = 10f;
    [SerializeField]
    private float minDistance = 1f;
    [SerializeField]
    private float maxDistance = 10f;
    [SerializeField]
    private float distance = 3;
    //CameraSpeed
    [SerializeField]
    private float sensitivity = 0.1f;
    [SerializeField]
    private float lookSpeed = 360f;
    [SerializeField]
    private float lookXLimit = 60.0f;
    //CameraCollision
    [SerializeField]
    private LayerMask layerMask;
    Vector2 rotation = Vector2.zero;

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;

        m_Input = Instantiate(m_InputAsset);
        m_CurrentActionMap = m_Input.FindActionMap("Default");
        m_MouseAction = m_CurrentActionMap.FindAction("Mouse");
        m_ZoomAction = m_CurrentActionMap.FindAction("MouseZoom");
        m_CurrentActionMap.Enable();

        rotation.y = camera.transform.eulerAngles.y;
    }

    void LateUpdate()
    {
        // Camera zoom
        distance -= m_ZoomAction.ReadValue<Vector2>().y * zoomSpeed * Time.deltaTime;
        distance = Mathf.Clamp(distance, minDistance, maxDistance);

        // Player and Camera rotation
        if (canMove)
        {
            //Orbitar
            //rotation.y += Input.GetAxis("Mouse X") * lookSpeed * sensitivity * Time.deltaTime;
            //rotation.x += -Input.GetAxis("Mouse Y") * lookSpeed * sensitivity * Time.deltaTime;
            //rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
            //camera.transform.eulerAngles = rotation; 
            input = m_MouseAction.ReadValue<Vector2>();
            rotation.y += input.x * lookSpeed * sensitivity * Time.deltaTime;
            rotation.x += -input.y * lookSpeed * sensitivity * Time.deltaTime;
            rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
            camera.transform.localEulerAngles = rotation;
            Vector3 mouseX = Vector3.up * input.x * lookSpeed * sensitivity * Time.deltaTime;
            player.transform.Rotate(mouseX);
        }

        RaycastHit hit;
        if (Physics.Raycast(player.transform.position, -camera.transform.forward, out hit, distance, layerMask))
        {
            camera.transform.position = hit.point + (camera.transform.forward * 0.1f);
            if (debug)
                Debug.DrawLine(camera.transform.position, hit.point, Color.blue, 2f);
        }
        else
            camera.transform.position = player.position - (camera.transform.forward * distance);

        if (Physics.Raycast(camera.transform.position, (offsetXnegate ? -1 : 1) * camera.transform.right, out hit, offsetX + offsetX, layerMask))
        {
            camera.transform.position = hit.point - ((offsetXnegate ? -1 : 1) * camera.transform.right * 0.8f);
            if (debug)
                Debug.DrawLine(camera.transform.position, hit.point, Color.red, 2f);
        }
        else
            camera.transform.position += (offsetXnegate ? -1 : 1) * camera.transform.right * offsetX;

        if (Physics.Raycast(camera.transform.position, camera.transform.up, out hit, offsetY + offsetY, layerMask))
        {
            camera.transform.position = hit.point - (camera.transform.up * 0.8f);
            if (debug)
                Debug.DrawLine(camera.transform.position, hit.point, Color.green, 2f);
        }
        else
            camera.transform.position += camera.transform.up * offsetY;

    }

    //-------------------------------------------//

    public void NegateX()
    {
        if(debug)
            Debug.Log("ChangeCamera");
        offsetXnegate = !offsetXnegate;

    }
}
