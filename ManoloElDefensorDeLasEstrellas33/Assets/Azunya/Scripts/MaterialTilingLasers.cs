using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MaterialTilingLasers : MonoBehaviour
{
    [SerializeField] private float tiling = 10;
    private void Start()
    {
        float scaleX = tiling * transform.localScale.x;
        float scaleZ = tiling * transform.localScale.z;
        Material material = GetComponent<MeshRenderer>().material;
        material.SetVector("_TilingGrid", new Vector2(scaleX, scaleZ));
    }

}
