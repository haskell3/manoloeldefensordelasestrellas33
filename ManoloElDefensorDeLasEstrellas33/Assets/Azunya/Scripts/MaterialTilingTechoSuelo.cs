using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTilingTechoSuelo: MonoBehaviour
{
    [SerializeField] private float tiling = 2;
    private void Awake()
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        Vector2[] uvs = mesh.uv;
        Vector2[] uvs1 = mesh.uv3;
        Vector2[] uvs2 = mesh.uv4;
        float scaleX = transform.localScale.x/tiling;
        float scaleZ = transform.localScale.z/tiling;
        for (int i = 0; i < uvs.Length; i++) {
            if (uvs[i].x == 1)
                uvs[i].x = scaleX;
            if (uvs[i].y == 1)
                uvs[i].y = scaleZ;
        }
        
        mesh.uv = uvs;
        mesh.uv3 = uvs1;
        mesh.uv4 = uvs2;
    }

}
