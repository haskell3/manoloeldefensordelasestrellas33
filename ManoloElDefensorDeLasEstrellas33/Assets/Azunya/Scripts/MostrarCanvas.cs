using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MostrarCanvas : MonoBehaviour
{
    [SerializeField]
    private GameObject[] activar;
    [SerializeField]
    private GameObject[] desactivar;
    void Awake()
    {
        foreach (GameObject activar in activar)
            activar.SetActive(false);
    }

    void Start()
    {
        
    }

    public void mostrar()
    {
        foreach (GameObject activar in activar)
            activar.SetActive(true);
    }

    public void noMostrar()
    {
        foreach (GameObject desactivar in desactivar)
            desactivar.SetActive(false);
    }
}
