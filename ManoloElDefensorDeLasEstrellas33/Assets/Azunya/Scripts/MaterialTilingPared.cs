using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTilingPared : MonoBehaviour
{
    [SerializeField] private float tiling = 2;
    [SerializeField] private MeshFilter meshFilterer;
    private void Awake()
    {

        Mesh mesh;
        if (meshFilterer)
            mesh = meshFilterer.mesh;
        else
            mesh = GetComponent<MeshFilter>().mesh;

        Vector2[] uvs = mesh.uv;
        float scaleX = Mathf.Max(transform.localScale.x, transform.localScale.z)/tiling;
        float scaleY = transform.localScale.y/tiling;
        for (int i = 0; i < uvs.Length; i++) {
            if (uvs[i].x == 1)
                uvs[i].x = scaleX;
            if (uvs[i].y == 1)
                uvs[i].y = scaleY;
        }
        
        mesh.uv = uvs;
        mesh.uv3 = uvs;
        mesh.uv4 = uvs;
    }

}
