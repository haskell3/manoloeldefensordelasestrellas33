using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ragdoll : MonoBehaviour
{
    private Rigidbody m_Rigibody;
    [SerializeField] private Rigidbody[] m_Bones;
    private void Awake()
    {
        m_Rigibody = GetComponent<Rigidbody>();
        m_Bones = GetComponentsInChildren<Rigidbody>();
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        m_Rigibody.isKinematic = !state;
        foreach (Collider hit in GetComponentsInChildren<SphereCollider>())
            hit.enabled = !state;
    }

}
