using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IExplosable
{
    public void Explode(Vector3 force);
}
