using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.tvOS;
using UnityEngine.VFX;
using static UnityEditor.ShaderGraph.Internal.KeywordDependentCollection;

public class MovimentPlayer : MonoBehaviour, IExplosable
{
    [Header("ScriptableObjects")]
    [SerializeField]
    private Options options;
    [Header("Layers")]
    [SerializeField]
    private LayerMask m_FloorLayer;
    [SerializeField]
    private LayerMask m_ShootMask;
    [SerializeField]
    private LayerMask m_ExplosionMask;
    [Header("GameObjects")]
    [SerializeField]
    private GameObject m_Camera;
    [SerializeField]
    private GameObject m_VFXExplosion;
    [SerializeField]
    private InputActionAsset m_InputAsset;
    [SerializeField]
    private PunteroMomento puntero;
    [Header("GameEvents")]
    [SerializeField]
    private GameEventFloat ActiveCooldownInt;
    [SerializeField]
    private GameEvent GameStarted;
    [SerializeField]
    private GameEvent m_Interact;
    [SerializeField]
    private GameEvent PlayerDead;
    [SerializeField]
    private GameEvent OpenOptions;
    [SerializeField]
    private GameEvent CloseOptions;

    private InputActionAsset m_Input;
    private InputAction m_MovementAction;
    private Rigidbody m_Rigidbody;
    private Vector2 m_Movement = Vector2.zero;

    [Header("Character Values")]
    [SerializeField]
    private float m_Speed = 2;
    [SerializeField]
    private float m_Acceleration = 2;
    [SerializeField]
    private float m_Drag = 2;
    [SerializeField]
    private float m_DragAir = 2;
    [SerializeField]
    private float m_JumpForce = 2;
    [SerializeField]
    private float m_JumpLess = 2;
    [SerializeField]
    private int m_JumpLoops = 2;
    [SerializeField]
    private float m_Gravity = 2;
    [SerializeField]
    private float m_RadiusExplosion = 2;
    [SerializeField]
    private float m_ExplosionPower = 2;
    [SerializeField]
    private float m_ClimbingForce = 2.5f;
    [SerializeField]
    private float m_ClimbingAcceleration = 2.5f;
    [SerializeField]
    private float cooldown = 3;
    [SerializeField]
    private float ReboundPower = 2;
    [SerializeField]
    private Color m_Color;

    [Header("Hook Values")]
    [SerializeField]
    private Vector3 grapplePoint;
    [SerializeField]
    private LayerMask whatIsGrappleable;
    [SerializeField]
    private float maxDistance = 100f;
    [SerializeField]
    private float spring = 4.5f;
    [SerializeField]
    private float damper = 7f;
    [SerializeField]
    private float massScale = 4.5f;
    [SerializeField]
    private float breakForce = 4.5f;

    


    private SpringJoint joint;
    private LineRenderer lr;
    private Vector3 m_CurrentGrapplePosition;

    private bool m_CanJump = true;
    private bool cannonactive = true;
    private Vector3 lastForce;
    private Transform m_EscalableObject;
    private enum SwitchMachineStates { NONE, IDLE, WALK, FLYING, CLIMBING };
    private SwitchMachineStates m_CurrentState;

    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;

        ExitState();
        InitState(newState);
        Debug.Log(newState.ToString());
    }

    private void InitState(SwitchMachineStates currentState)
    {
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                //float yVelocity = m_Rigidbody.velocity.y;
                //m_Rigidbody.velocity = new Vector3( 0, yVelocity, 0);

                break;

            case SwitchMachineStates.WALK:

                break;
            case SwitchMachineStates.FLYING:

                break;
            case SwitchMachineStates.CLIMBING:

                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:

                break;

            case SwitchMachineStates.WALK:

                break;
            case SwitchMachineStates.FLYING:

                break;
            case SwitchMachineStates.CLIMBING:

                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if (m_Movement != Vector2.zero)
                    ChangeState(SwitchMachineStates.WALK);
                break;
            case SwitchMachineStates.WALK:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                if (m_Movement == Vector2.zero)
                    ChangeState(SwitchMachineStates.IDLE);
                break;
            case SwitchMachineStates.FLYING:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                break;
            case SwitchMachineStates.CLIMBING:
                m_Movement = m_MovementAction.ReadValue<Vector2>();
                break;
            default:
                break;
        }

    }
    private void FixedUpdate()
    {
        Vector3 groundSpeed = Vector3.right * m_Rigidbody.velocity.x + Vector3.forward * m_Rigidbody.velocity.z;
        Vector3 movement = (transform.forward * m_Movement.y + transform.right * m_Movement.x).normalized;

        if (m_CurrentState == SwitchMachineStates.CLIMBING)
        {
            if (m_Movement.y != 0 && Math.Abs(m_Rigidbody.velocity.y) < m_Speed)
            {
                Vector3 cross = Vector3.Cross(m_EscalableObject.forward, transform.forward).normalized;
                m_Rigidbody.AddForce((cross.y < 0 ? -1 : 1) * cross * m_ClimbingForce * m_Movement.y, ForceMode.Acceleration);
            }
            m_Rigidbody.AddForce(m_Movement.x * m_EscalableObject.right * -1 * m_Speed * m_ClimbingAcceleration, ForceMode.Acceleration);
            if (m_Rigidbody.velocity.y != 0 && m_Movement.y == 0)
                m_Rigidbody.AddForce(-Vector3.up * m_Rigidbody.velocity.y, ForceMode.VelocityChange);

        }
        else
        {
            if (groundSpeed.magnitude < m_Speed)
            {
                if (!Physics.Raycast(transform.position, movement, .7f, m_ShootMask))
                {
                    m_Rigidbody.AddForce((movement * m_Speed) * m_Acceleration, ForceMode.Acceleration);
                }
            }
            //Gravity
            m_Rigidbody.AddForce(Vector3.up * -1 * m_Gravity, ForceMode.Acceleration);
        }

        groundSpeed = Vector3.right * m_Rigidbody.velocity.x + Vector3.forward * m_Rigidbody.velocity.z;
        //Floor drag
        if (m_CurrentState != SwitchMachineStates.FLYING)
            m_Rigidbody.AddForce(-groundSpeed * m_Drag, ForceMode.Acceleration);
        else
            m_Rigidbody.AddForce(-groundSpeed * m_DragAir, ForceMode.Acceleration);
        //Air drag ^^
    }

    void Awake()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Assert.IsNotNull(m_InputAsset);

        m_Input = Instantiate(m_InputAsset);
        m_MovementAction = m_Input.FindActionMap("Default").FindAction("Movement");
        m_Input.FindActionMap("Default").FindAction("Jump").started += StartJumping;
        m_Input.FindActionMap("Default").FindAction("Jump").canceled += EndJumping;
        m_Input.FindActionMap("Default").FindAction("ChangeCamera").started += changeCamera;
        m_Input.FindActionMap("Default").FindAction("Explosion").performed += Explosion;
        m_Input.FindActionMap("Default").FindAction("Hook").performed += StartGrapple;
        m_Input.FindActionMap("Default").FindAction("Hook").canceled += StopGrapple;
        m_Input.FindActionMap("Default").FindAction("Interact").performed += Interact;
        m_Input.FindActionMap("Default").FindAction("OpenOptions").performed += changeToOptions;
        m_Input.FindActionMap("Menu").FindAction("Start").performed += changeActionMap;
        m_Input.FindActionMap("Menu").FindAction("CloseOptions").performed += closeOptions;


        m_Input.FindActionMap("Menu").Enable();

        m_Rigidbody = GetComponent<Rigidbody>();

        lr = GetComponent<LineRenderer>();

        m_Color = options.playerColor;
        gameObject.GetComponent<MeshRenderer>().material.color = m_Color;
    }

    private void Explosion(InputAction.CallbackContext context)
    {
        if (cannonactive)
        {
            RaycastHit hit;

            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, Mathf.Infinity, m_ShootMask))
            {
                GameObject explo = Instantiate(m_VFXExplosion);
                explo.transform.position = hit.point;
                RaycastHit[] hits = Physics.SphereCastAll(hit.point, m_RadiusExplosion, Vector3.up, 1, m_ExplosionMask);

                foreach (RaycastHit exploded in hits)
                {
                    Debug.DrawLine(exploded.rigidbody.transform.position, hit.point, Color.red, 3f);
                    IExplosable ie = exploded.rigidbody.GetComponentInParent<IExplosable>();

                    if (ie == null) continue;

                    Vector3 power = exploded.rigidbody.transform.position - hit.point;

                    power = power.normalized;

                    if (exploded.distance > 0)
                    {
                        power *= (m_ExplosionPower * (m_RadiusExplosion / exploded.distance));
                    }
                    else
                    {
                        power *= (m_ExplosionPower * m_RadiusExplosion);
                    }


                    ie.Explode(power);
                }
            }
            cannonactive = false;
            ActiveCooldownInt.Raise(cooldown);
        }
    }

    private void EndJumping(InputAction.CallbackContext context)
    {
        StopCoroutine(MoreJump());
    }

    private void StartJumping(InputAction.CallbackContext context)
    {
        if (!m_CanJump) return;
        if (m_CurrentState == SwitchMachineStates.FLYING)
        {
            m_CanJump = false;
        }
        ChangeState(SwitchMachineStates.FLYING);
        StartCoroutine(MoreJump());
    }

    private IEnumerator MoreJump()
    {
        float mult = 1f;
        for (int i = m_JumpLoops; i >= 0; i--)
        {
            m_Rigidbody.AddForce(transform.up * m_JumpForce * mult);
            mult -= m_JumpLess;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void changeCamera(InputAction.CallbackContext context)
    {
        m_Camera.GetComponent<TestCamera>().NegateX();
    }

    private void changeActionMap(InputAction.CallbackContext context)
    {
        m_Input.FindActionMap("Menu").Disable();
        m_Input.FindActionMap("Default").Enable();
        m_Camera.GetComponent<TestCamera>().enabled = true;
        GameStarted?.Raise();
        StartCoroutine(CheckHookable());
    }

    private void Start()
    {
        InitState(SwitchMachineStates.IDLE);
    }

    void Update()
    {
        UpdateState();
        if (m_Rigidbody.velocity != Vector3.zero) lastForce = m_Rigidbody.velocity;

    }

    private void OnDestroy()
    {
        m_Input.FindActionMap("Default").Disable();
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Layer from floors
        if (collision.gameObject.layer == 3)
        {
            ChangeState(SwitchMachineStates.WALK);
            m_CanJump = true;
        }
        //Layer from walls
        if (collision.gameObject.layer == 7)
        {
            Debug.Log("Te la devuelvo " + lastForce);
            if (lastForce.x > 12 || lastForce.z > 12 || lastForce.y > 12)
            {
                Debug.Log("Boing");
                //var direction = Vector3.Reflect(lastForce.normalized, collision.contacts[0].normal);
                var direction = Vector3.Reflect(lastForce.normalized, collision.GetContact(0).normal);
                //Vector3 rebote = new Vector3(lastForce.x * -ReboundPower, lastForce.y * ReboundPower, lastForce.z * -ReboundPower);
                Vector3 rebote = lastForce.magnitude * direction * ReboundPower;
                m_Rigidbody.AddForce(rebote, ForceMode.Impulse);
            }

        }
        //Layers from lasers
         if (collision.gameObject.layer == 10)
        {
            StopGrapple();
            PlayerDead?.Raise();
        }
        //Layer from escalable
        if (collision.gameObject.layer == 11)
        {
            if (Vector3.Dot(transform.forward.normalized, collision.transform.forward.normalized) < -0.9)
            {
                m_EscalableObject = collision.transform;
                ChangeState(SwitchMachineStates.CLIMBING);
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //Layer from floors                                                                             Layer from escalable walls
        if ((collision.gameObject.layer == 3 && m_CurrentState != SwitchMachineStates.CLIMBING) || collision.gameObject.layer == 11)
        {
            ChangeState(SwitchMachineStates.FLYING);
        }
    }

    public void Explode(Vector3 force)
    {
        m_Rigidbody.AddForce(force, ForceMode.Impulse);
    }

    public void ActivateCannon()
    {
        cannonactive = true;
    }

    //Called after Update
    void LateUpdate()
    {
        DrawRope();
    }



    void StartGrapple(InputAction.CallbackContext context)
    {
        //Debug.Log("Grabbing");
        RaycastHit hit;
        if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, maxDistance, whatIsGrappleable))
        {
            if (hit.collider.gameObject.layer != 7)
            {
                grapplePoint = hit.point;
                joint = gameObject.AddComponent<SpringJoint>();
                joint.autoConfigureConnectedAnchor = false;
                joint.connectedAnchor = grapplePoint;
                joint.breakForce = breakForce;

                float distanceFromPoint = Vector3.Distance(transform.position, grapplePoint);

                joint.maxDistance = distanceFromPoint * 0.8f;
                joint.minDistance = distanceFromPoint * 0.25f;
                joint.spring = spring;
                joint.damper = damper;
                joint.massScale = massScale;

                lr.positionCount = 2;
                m_CurrentGrapplePosition = transform.position;
            }
            
        }
    }

    void StopGrapple(InputAction.CallbackContext context)
    {
        //Debug.Log("Stop it");
        lr.positionCount = 0;
        Destroy(joint);
    }

    void StopGrapple()
    {
        //Debug.Log("Stop it");
        lr.positionCount = 0;
        Destroy(joint);
    }

    void Interact(InputAction.CallbackContext context)
    {
        m_Interact.Raise();
    }

    void DrawRope()
    {
        //If not grappling, don't draw rope
        if (!joint) return;

        m_CurrentGrapplePosition = Vector3.Lerp(m_CurrentGrapplePosition, grapplePoint, Time.deltaTime * 8f);

        lr.SetPosition(0, transform.position);
        lr.SetPosition(1, m_CurrentGrapplePosition);
    }

    public bool IsGrappling()
    {
        return joint != null;
    }

    public Vector3 GetGrapplePoint()
    {
        return grapplePoint;
    }

    IEnumerator CheckHookable()
    {
        while (true)
        {
            RaycastHit hit;
            if (Physics.Raycast(m_Camera.transform.position, m_Camera.transform.forward, out hit, maxDistance, whatIsGrappleable))
            {
                if (hit.collider.gameObject.layer != 7)
                    puntero.IsHookable(true);
                else
                    puntero.IsHookable(false);
            } else
                puntero.IsHookable(false);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void OnJointBreak(float breakForce)
    {
        lr.positionCount = 0;
        Destroy(joint);
    }

    void changeToOptions(InputAction.CallbackContext context)
    {
        m_Input.FindActionMap("Default").Disable();
        m_Input.FindActionMap("Menu").Enable();
        
        Cursor.lockState = CursorLockMode.Confined;
        OpenOptions.Raise();
    }

    void closeOptions(InputAction.CallbackContext context)
    {
        m_Color = options.playerColor;
        gameObject.GetComponent<MeshRenderer>().material.color = m_Color;

        m_Input.FindActionMap("Menu").Disable();
        m_Input.FindActionMap("Default").Enable();

        Cursor.lockState = CursorLockMode.Locked;
        CloseOptions.Raise();

    }
}